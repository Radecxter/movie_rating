FROM webgoal/generic_ror:latest as development

RUN	touch /usr/local/bin/cls
RUN	echo '#!/bin/sh' > /usr/local/bin/cls
RUN	echo 'echo -e \\033c' >> /usr/local/bin/cls
RUN	chmod +x /usr/local/bin/cls
CMD bash start.sh

FROM development as production
COPY . /usr/src/app
RUN	bundle install
RUN	rake assets:precompile
