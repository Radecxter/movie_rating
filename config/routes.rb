Rails.application.routes.draw do
  root "movies#index"
  resources :movies
  resources :rates
  resources :comments
  resources :users

  post "/login", to: "users#login"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
