class CreateMovies < ActiveRecord::Migration[5.1]
  def change
    create_table :movies do |t|
      t.string :name
      t.string :gender
      t.integer :year
      t.integer :minutes

      t.timestamps
    end
  end
end
