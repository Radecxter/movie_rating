json.extract! rate, :id, :score, :movie_id, :user_id, :created_at, :updated_at
json.url rate_url(rate, format: :json)
