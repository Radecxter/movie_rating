json.extract! movie, :id, :name, :description, :gender, :director, :year, :minutes, :rates_average

json.comments do
  json.array! @comments, partial: 'comments/comment', as: :comment
end

json.url movie_url(movie, format: :json)
