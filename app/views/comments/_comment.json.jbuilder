json.extract! comment, :id, :commentary, :movie_id

json.user do
  json.id comment.user.id
  json.name comment.user.name
  json.username comment.user.username
end
