class Movie < ApplicationRecord
  has_many :comments
  has_many :rates

  validates :name,
            :year,
            :gender,
            :director,
            :minutes,
            presence: true

  def rates_average
      self.rates.average(:score).to_f
  end
end
